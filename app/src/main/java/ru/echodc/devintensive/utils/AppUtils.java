package ru.echodc.devintensive.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class AppUtils {

  private static final Context sContext = DevintensiveApplication.getContext();

  /**
   * @return Высота Action bar в текущем контексте
   */
  public static float getAppBarSize() {
    float appBarSize;
    final TypedArray styledAttributes = sContext.getTheme().obtainStyledAttributes(
        new int[]{android.R.attr.actionBarSize});
    appBarSize = styledAttributes.getDimension(0, 0);
    styledAttributes.recycle();
    return appBarSize;
  }

  /**
   * @param v view
   * @return минимальная высота View, которую это View должно обернуть своим контентом
   */
  public static int getViewMinHeight(View v) {
    int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(getScreenWidth(), View.MeasureSpec.AT_MOST);
    int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    v.measure(widthMeasureSpec, heightMeasureSpec);
    return v.getMeasuredHeight();
  }

  public static int getScreenWidth() {
    WindowManager wm = (WindowManager) sContext.getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    return size.x;
  }

  /**
   * @return высота StatusBar в текущем контексте
   */
  public static int getStatusBarHeight() {
    int result = 0;
    int resourceId = sContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
    if (resourceId > 0) {
      result = sContext.getResources().getDimensionPixelSize(resourceId);
    }
    return result;
  }

  // region ===================== Другая реализация для класса UserInfoBehavior=====================

  //  /**
  //   * @return высота StatusBar в текущем контексте
  //   */
  //  public static int getStatusBarHeight() {
  //    int result = 0;
  //    int resourceId = sContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
  //    if (resourceId > 0) {
  //      result = sContext.getResources().getDimensionPixelSize(resourceId);
  //    }
  //    return result;
  //  }

  public static int getAppBarHeight() {
    int result = 0;
    TypedValue typedValue = new TypedValue();
    if (sContext.getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)) {
      result = TypedValue.complexToDimensionPixelSize(typedValue.data, sContext.getResources().getDisplayMetrics());
    }
    return result;
  }

  /**
   * @param start = минимальное значение в пикселях
   * @param end = максимальное значение в пикселях
   * @param friction = текщуее положение
   */
  //  Апроксимируем чтобы узнать текущее значение
  public static int lerp(int start, int end, float friction) {
    return (int) (start + (end - start) * friction);
  }

  /**
   * @param start = минимальный размер
   * @param end = максимальный размер
   * @param currentValue = текущее положение нижнего края AppBarLayout
   */
  //  Чтобы понимать в каком положении находится AppBarLayout в своем максимально скрытом состоянии
  public static float currentFriction(int start, int end, int currentValue) {
    return (currentValue - start) / (end - start);
  }

  // endregion Другая реализация


}
