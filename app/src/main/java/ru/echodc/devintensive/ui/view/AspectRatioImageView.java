package ru.echodc.devintensive.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import ru.echodc.devintensive.R;

public class AspectRatioImageView extends ImageView {

  private static final float DEFAULT_ASPECT_RATIO = 1.7f; // 1.78 = 16:9
  private final float mAspectRatio;

  public AspectRatioImageView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);

    //    Получаем кастомные атрибуты
    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
    mAspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
    a.recycle();
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    int newWidth;
    int newHeight;

    //    Получаем ширину карточки
    newWidth = getMeasuredWidth();
    //    Вычисляем высоту карточки из ширины и отношению сторон
    newHeight = (int) (newWidth / mAspectRatio);

    //    Устанавливаем новые значения
    setMeasuredDimension(newWidth, newHeight);
  }
}
