package ru.echodc.devintensive.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.echodc.devintensive.R;
import ru.echodc.devintensive.data.managers.DataManager;
import ru.echodc.devintensive.data.network.request.UserLoginRequest;
import ru.echodc.devintensive.data.network.response.UserListResponse;
import ru.echodc.devintensive.data.network.response.UserModelResponse;
import ru.echodc.devintensive.data.storage.models.Repository;
import ru.echodc.devintensive.data.storage.models.RepositoryDao;
import ru.echodc.devintensive.data.storage.models.User;
import ru.echodc.devintensive.data.storage.models.UserDao;
import ru.echodc.devintensive.utils.ConstantManager;
import ru.echodc.devintensive.utils.NetworkStatusChecker;

public class AuthActivity extends Activity {

  private static final String TAG = ConstantManager.TAG_PREFIX + " AuthActivity";

  @BindView(R.id.main_coordinator_container)
  CoordinatorLayout mMainCoordinatorContainer;
  @BindView(R.id.authorization_box)
  CardView mAuthorizationBox;
  @BindView(R.id.login_indicator)
  ImageView mLoginIndicator;
  @BindView(R.id.login_txt)
  TextView mLoginTxt;
  @BindView(R.id.wrapper_login_email)
  TextInputLayout mWrapperLoginEmail;
  @BindView(R.id.wrapper_login_password)
  TextInputLayout mWrapperLoginPassword;
  @BindView(R.id.login_email_et)
  EditText mLoginEmailEt;
  @BindView(R.id.login_password_et)
  EditText mLoginPasswordEt;
  @BindView(R.id.login_btn)
  Button mLoginBtn;
  @BindView(R.id.remember_password_txt)
  TextView mRememberPasswordTxt;

  private DataManager mDataManager;

  private RepositoryDao mRepositoryDao;
  private UserDao mUserDao;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_auth);
    ButterKnife.bind(this);

    mDataManager = DataManager.getInstance();
    mUserDao = mDataManager.getDaoSession().getUserDao();
    mRepositoryDao = mDataManager.getDaoSession().getRepositoryDao();
  }

  // region ===================== Events =====================
  @OnClick(R.id.login_btn)
  public void loginButtonClick() {
    signIn();
  }

  @OnClick(R.id.remember_password_txt)
  public void rememberPasswordClick() {
    rememberPassword();
  }

  private void showSnackBar(String message) {
    Snackbar.make(mMainCoordinatorContainer, message, Snackbar.LENGTH_LONG).show();
  }

  private void rememberPassword() {
    Intent rememberIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://devintensive.softdesign-apps.ru/forgotpass"));
    startActivity(rememberIntent);
  }

  private void loginSuccess(UserModelResponse userModelResponse) {

    showSnackBar(userModelResponse.getData().getToken());
    //    В ответе от сервера получаем Токен и ID пользователя и сохраняем их в PreferencesManager
    mDataManager.getPreferencesManager().saveAuthToken(userModelResponse.getData().getToken());
    mDataManager.getPreferencesManager().saveUserId(userModelResponse.getData().getUser().getId());
    saveUserValues(userModelResponse);
    //    saveUserData(userModelResponse);

    Intent loginIntent = new Intent(this, MainActivity.class);
    startActivity(loginIntent);
  }

  private void signIn() {
    //    Проверяем состояние сети
    if (NetworkStatusChecker.isNetworkAviable(this)) {

      //      Вызов с передачей динамического заголовка
      //      Call<UserModelResponse> call = mDataManager.loginUser(new Date().toString(), new UserLoginRequest(mLoginEmailEt.getText().toString(), mLoginPasswordEt.getText().toString()));
      Call<UserModelResponse> call = mDataManager.loginUser(new UserLoginRequest(mLoginEmailEt.getText().toString(), mLoginPasswordEt.getText().toString()));
      call.enqueue(new Callback<UserModelResponse>() {
        @Override
        public void onResponse(@NonNull Call<UserModelResponse> call, @NonNull Response<UserModelResponse> response) {
          //        Если код = 200, то значит логирование прошло успешно
          if (response.code() == 200) {
            loginSuccess(response.body());
          } else if (response.code() == 403) {
            showSnackBar("Неверный логин или пароль!");
          } else {
            showSnackBar("Неизвестная ошибка!!");
          }
        }

        @Override
        public void onFailure(Call<UserModelResponse> call, Throwable t) {
          // TODO: 20.12.2017 Обработать ошибки
        }
      });
    } else {
      showSnackBar("Сеть в данный момент не доступна. Попробуйте позже!");
    }
  }
  // endregion Events

  //  Сохранение полей из запроса к серверу
  private void saveUserValues(UserModelResponse userModelResponse) {
    int[] userValues = {
        userModelResponse.getData().getUser().getProfileValues().getRating(),
        userModelResponse.getData().getUser().getProfileValues().getLinesCode(),
        userModelResponse.getData().getUser().getProfileValues().getProjects(),
    };

    //    Сохраняем, полученные данные для шапки с данными
    mDataManager.getPreferencesManager().saveUserProfileValues(userValues);
    //    Сохраняем, полученную фотку для пользователя
    mDataManager.getPreferencesManager().saveUserPhoto(Uri.parse(userModelResponse.getData().getUser().getPublicInfo().getPhoto()));
  }

  private void saveUserInDb() {
    Call<UserListResponse> call = mDataManager.getUserListFromNetwork();

    call.enqueue(new Callback<UserListResponse>() {
      @Override
      public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
        try {
          if (response.code() == 200) {
            List<Repository> allRepositories = new ArrayList<Repository>();
            List<User> allUser = new ArrayList<>();

            //            Получаем в цикле по одному пользователю
            for (UserListResponse.UserData userRes : response.body().getUserData()) {
              //              Через getRepoListFromUserRes возвращается коллекция репозиториев пользователя
              allRepositories.addAll(getRepoListFromUserRes(userRes));
              allUser.add(new User(userRes));
            }

            mRepositoryDao.insertOrReplaceInTx(allRepositories);
            mUserDao.insertOrReplaceInTx(allUser);

          } else {
            showSnackbar("Сптсок пользователей не может быть получен");
            Log.e(TAG, "onResponse: " + String.valueOf(response.errorBody().source()));
          }

        } catch (NullPointerException e) {
          e.printStackTrace();
          Log.e(TAG, e.toString());
          showSnackbar("Что-то не так!");
        }
      }

      @Override
      public void onFailure(Call<UserListResponse> call, Throwable t) {
        // TODO: 25.12.2017 обработать ошибки
      }
    });
  }

  private void showSnackbar(String message) {
    Snackbar.make(mMainCoordinatorContainer, message, Snackbar.LENGTH_LONG).show();
  }

  private List<Repository> getRepoListFromUserRes(UserListResponse.UserData userData) {
    final String userId = userData.getId();

    List<Repository> repositories = new ArrayList<>();
    for (UserModelResponse.Repo repositoryRes : userData.getRepositories().getRepo()) {
      repositories.add(new Repository(repositoryRes, userId));
    }

    return repositories;
  }

}
