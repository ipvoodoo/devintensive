package ru.echodc.devintensive.ui.view.behaviors;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import ru.echodc.devintensive.R;
import ru.echodc.devintensive.utils.AppUtils;

/**
 * Класс не используется, т.е. наблюдается дерганье при скроллинге
 */
public class UserInfoBehavior<V extends LinearLayout> extends AppBarLayout.ScrollingViewBehavior {

  private final int mMaxAppbarHeight;
  private final int mMinAppbarHeight;
  private final int mMaxUserInfoHeight;
  private final int mMinUserInfoHeight;

  public UserInfoBehavior(Context context, AttributeSet attrs) {
    super(context, attrs);
    //    Кастомные атрибуты
    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.UserInfoBehavior);
    mMinUserInfoHeight = a.getDimensionPixelSize(R.styleable.UserInfoBehavior_behavior_min_height, 56);
    a.recycle();
    mMinAppbarHeight = AppUtils.getStatusBarHeight() + AppUtils.getAppBarHeight();   //80dp
    mMaxAppbarHeight = context.getResources().getDimensionPixelSize(R.dimen.size_profile_image);   //256dp
    mMaxUserInfoHeight = context.getResources().getDimensionPixelSize(R.dimen.user_info_size);             //112dp
  }

  //  Будет вызываться каждый раз при изменении размера AppBarLayout
  @Override
  public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {

    //    Определяем текущее положение
    float currentFriction = AppUtils.currentFriction(mMinAppbarHeight, mMaxAppbarHeight, dependency.getBottom());
    int currentHeight = AppUtils.lerp(mMinUserInfoHeight, mMaxUserInfoHeight, currentFriction);
    //    Получаем текщие параметры CoordinatorLayout
    CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
    lp.height = currentHeight;
    child.setLayoutParams(lp);

    return super.onDependentViewChanged(parent, child, dependency);
  }

  @Override
  public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
    //    Отслеживаем зависимость от AppBarLayout
    return dependency instanceof AppBarLayout;
  }
}
