package ru.echodc.devintensive.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import java.util.List;
import ru.echodc.devintensive.R;
import ru.echodc.devintensive.data.managers.DataManager;
import ru.echodc.devintensive.data.network.response.UserListResponse;
import ru.echodc.devintensive.data.network.response.UserListResponse.UserData;
import ru.echodc.devintensive.ui.adapters.UserAdapter.UserViewHolder.CustomClickListener;
import ru.echodc.devintensive.ui.view.AspectRatioImageView;
import ru.echodc.devintensive.utils.ConstantManager;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

  private static final String TAG = ConstantManager.TAG_PREFIX + "UserAdapter ";
  //  Список данных по всем пользователям
  private List<UserData> mUsers;

  private Context mContext;

  private CustomClickListener mClickListener;


  public UserAdapter(List<UserData> users, CustomClickListener customClickListener) {
    mUsers = users;
    this.mClickListener = customClickListener;
  }


  @Override
  public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    mContext = parent.getContext();
    View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_user_list, parent, false);

    return new UserViewHolder(convertView, mClickListener);
  }

  @Override
  public void onBindViewHolder(final UserViewHolder holder, int position) {
    final UserListResponse.UserData user = mUsers.get(position);
    final String userPhoto;
    if (user.getPublicInfo().getPhoto().isEmpty()) {
      userPhoto = "null";
      Log.e(TAG, "onBindViewHolder: user with name: " + user.getFullName() + " has empty name");
    } else {
      userPhoto = user.getPublicInfo().getPhoto();
    }

    //    Контекст нужен только для Picasso
    DataManager.getInstance().getPicasso()
        .load(userPhoto)
        .error(holder.mDummy)
        .placeholder(holder.mDummy)
        .fit()
        .centerCrop()
        .networkPolicy(NetworkPolicy.OFFLINE)
        .into(holder.mUserPhoto, new Callback() {
          @Override
          public void onSuccess() {
            Log.d(TAG, " load from cache");
          }

          @Override
          public void onError() {
            DataManager.getInstance().getPicasso()
                .load(userPhoto)
                .error(holder.mDummy)
                .placeholder(holder.mDummy)
                .fit()
                .centerCrop()
                .into(holder.mUserPhoto, new Callback() {
                  @Override
                  public void onSuccess() {

                  }

                  @Override
                  public void onError() {
                    Log.d(TAG, " Could not fetch image");
                  }
                });
          }
        });

    holder.mUserFullNameTxt.setText(user.getFullName());
    holder.mRatingTxt.setText(String.valueOf(user.getProfileValues().getRating()));
    holder.mCodeLinesTxt.setText(String.valueOf(user.getProfileValues().getLinesCode()));
    holder.mProjectsTxt.setText(String.valueOf(user.getProfileValues().getProjects()));

    if (user.getPublicInfo().getBio() == null || user.getPublicInfo().getBio().isEmpty()) {
      holder.mBioTxt.setVisibility(View.GONE);
    } else {
      holder.mBioTxt.setVisibility(View.VISIBLE);// - нужно чтобы видимость стала вновь доступной при скроллинге
      holder.mBioTxt.setText(user.getPublicInfo().getBio());
    }
  }

  @Override
  public int getItemCount() {
    return mUsers.size();
  }

  public static class UserViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_full_name_txt)
    TextView mUserFullNameTxt;
    @BindView(R.id.user_photo)
    AspectRatioImageView mUserPhoto;
    @BindView(R.id.rating_txt)
    TextView mRatingTxt;
    @BindView(R.id.code_lines_txt)
    TextView mCodeLinesTxt;
    @BindView(R.id.projects_txt)
    TextView mProjectsTxt;
    @BindView(R.id.bio_txt)
    TextView mBioTxt;
    @BindView(R.id.more_info_btn)
    Button mMoreInfoBtn;

    private CustomClickListener mListener;

    protected Drawable mDummy;


    UserViewHolder(View itemView, CustomClickListener customClickListener) {
      super(itemView);
      ButterKnife.bind(this, itemView);

      mDummy = mUserPhoto.getContext().getResources().getDrawable(R.drawable.login_bg);

      mListener = customClickListener;
    }

    //    Кастомный слушатель для переброса в Активити
    public interface CustomClickListener {

      void onUserItemClickListener(int position);
    }

    @OnClick(R.id.more_info_btn)
    void showMoreButtonClick() {
      if (mListener != null) {
        mListener.onUserItemClickListener(getAdapterPosition());
      }
    }
  }
}
