package ru.echodc.devintensive.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.echodc.devintensive.R;
import ru.echodc.devintensive.data.managers.DataManager;
import ru.echodc.devintensive.data.network.response.UserListResponse;
import ru.echodc.devintensive.data.network.response.UserListResponse.UserData;
import ru.echodc.devintensive.data.storage.models.UserDTO;
import ru.echodc.devintensive.ui.adapters.UserAdapter;
import ru.echodc.devintensive.ui.adapters.UserAdapter.UserViewHolder.CustomClickListener;
import ru.echodc.devintensive.utils.ConstantManager;

public class UserListActivity extends AppCompatActivity {

  private static final String TAG = ConstantManager.TAG_PREFIX + " UserListActivity";
  @BindView(R.id.coordinator_layout)
  CoordinatorLayout mCoordinatorLayout;
  @BindView(R.id.toolbar)
  Toolbar mToolbar;
  @BindView(R.id.drawer_layout)
  DrawerLayout mDrawerLayout;
  @BindView(R.id.user_list)
  RecyclerView mRecyclerView;

  private DataManager mDataManager;
  private UserAdapter mUserAdapter;
  private List<UserData> mUsers;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_user_list);
    ButterKnife.bind(this);

    mDataManager = DataManager.getInstance();

    GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
    mRecyclerView.setLayoutManager(gridLayoutManager);

    setupToolbar();
    setupDrawer();
    loadUsers();
  }

  private void showSnackbar(String message) {
    Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
  }

  private void loadUsers() {
    Call<UserListResponse> call = mDataManager.getUserListFromNetwork();

    call.enqueue(new Callback<UserListResponse>() {
      @Override
      public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
        try {
          mUsers = response.body().getUserData();
          mUserAdapter = new UserAdapter(mUsers, new CustomClickListener() {
            @Override
            public void onUserItemClickListener(int position) {
              //              Получаем список пользователей mUsers = response.body().getUserData(); и передаем в UserDTO
              //              Получаем позицию конкретного пользователя mUsers.get(position)
              UserDTO userDTO = new UserDTO(mUsers.get(position));

              Intent profileIntent = new Intent(UserListActivity.this, ProfileUserActivity.class);
              profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, userDTO);

              startActivity(profileIntent);
            }
          });
          mRecyclerView.setAdapter(mUserAdapter);
        } catch (Exception e) {
          e.printStackTrace();
          Log.e(TAG, e.toString());
          showSnackbar("Что-то не так!");
        }
      }

      @Override
      public void onFailure(Call<UserListResponse> call, Throwable t) {
        // TODO: 25.12.2017 обработать ошибки
      }
    });
  }

  private void setupDrawer() {
  }

  private void setupToolbar() {
    setSupportActionBar(mToolbar);
    ActionBar actionBar = getSupportActionBar();

    if (actionBar != null) {
      actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }
}
