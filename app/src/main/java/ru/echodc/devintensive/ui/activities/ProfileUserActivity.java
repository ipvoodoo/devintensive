package ru.echodc.devintensive.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import java.util.List;
import ru.echodc.devintensive.R;
import ru.echodc.devintensive.data.storage.models.UserDTO;
import ru.echodc.devintensive.ui.adapters.RepositoriesAdapter;
import ru.echodc.devintensive.utils.ConstantManager;

public class ProfileUserActivity extends AppCompatActivity {

  @BindView(R.id.toolbar)
  Toolbar mToolbar;
  @BindView(R.id.user_photo_img)
  ImageView mUserPhotoImg;
  @BindView(R.id.user_bio_et)
  EditText mUserBioEt;
  @BindView(R.id.tv_user_rating)
  TextView mTvUserRating;
  @BindView(R.id.tv_user_code_lines)
  TextView mTvUserCodeLines;
  @BindView(R.id.tv_user_projects)
  TextView mTvUserProjects;
  @BindView(R.id.repositories_list)
  ListView mRepoListView;

  @BindView(R.id.collapsing_toolbar)
  CollapsingToolbarLayout mCollapsingToolbar;

  @BindView(R.id.coordinator_layout)
  CoordinatorLayout mCoordinatorLayout;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile_user);

    ButterKnife.bind(this);

    setupToolbar();
    initProfileData();
  }

  private void setupToolbar() {
    setSupportActionBar(mToolbar);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }

  }

  private void initProfileData() {
    UserDTO userDTO = getIntent().getParcelableExtra(ConstantManager.PARCELABLE_KEY);

    final List<String> repositories = userDTO.getRepositories();
    final RepositoriesAdapter repositoriesAdapter = new RepositoriesAdapter(this, repositories);
    mRepoListView.setAdapter(repositoriesAdapter);

    mRepoListView.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Snackbar.make(mCoordinatorLayout, "Репозиторий " + repositories.get(position), Snackbar.LENGTH_LONG).show();
        // TODO: 09.01.2018 Реализовать просмотр репозитория через Intent.Action_View
      }
    });

    mUserBioEt.setText(userDTO.getBio());
    mTvUserRating.setText(userDTO.getRating());
    mTvUserCodeLines.setText(userDTO.getCodeLines());
    mTvUserProjects.setText(userDTO.getProjects());

    mCollapsingToolbar.setTitle(userDTO.getFullName());

    Picasso.with(this)
        .load(userDTO.getPhoto())
        .placeholder(R.drawable.userphoto)
        .error(R.drawable.userphoto)
        .into(mUserPhotoImg);
  }
}
