package ru.echodc.devintensive.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.List;
import ru.echodc.devintensive.R;

public class RepositoriesAdapter extends BaseAdapter{

  private List<String> mRepoList;
  private Context mContext;
  private LayoutInflater mLayoutInflater;

  @BindView(R.id.user_git_et)
  TextView mUserGitEt;

  public RepositoriesAdapter(Context context, List<String> repoList) {
    mRepoList = repoList;
    mContext = context;
    mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return mRepoList.size();
  }

  @Override
  public Object getItem(int position) {
    return mRepoList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View itemView = convertView;

    if (itemView == null){
      itemView = mLayoutInflater.inflate(R.layout.item_repositories_list, parent, false);
    }

    ButterKnife.bind(this, itemView);
    mUserGitEt.setText(mRepoList.get(position));
    return itemView;
  }
}
