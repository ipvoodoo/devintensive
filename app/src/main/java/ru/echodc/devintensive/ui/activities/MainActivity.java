package ru.echodc.devintensive.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.MediaColumns;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.LayoutParams;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import ru.echodc.devintensive.R;
import ru.echodc.devintensive.data.managers.DataManager;
import ru.echodc.devintensive.data.network.response.UserModelResponse;
import ru.echodc.devintensive.utils.ConstantManager;
import ru.echodc.devintensive.utils.ProfileDataTextWatcher;

public class MainActivity extends AppCompatActivity implements OnClickListener {


  private static final String TAG = "MainActivity ====> ";
  private DataManager mDataManager;

  //  Переключатель для режима Активити (редактирование / просмотр)
  private int mCurrentEditMode = 0;

  @BindView(R.id.drawer_layout)
  DrawerLayout mDrawerLayout;
  @BindView(R.id.navigation_view)
  NavigationView mNavigationView;
  @BindView(R.id.coordinator_layout)
  CoordinatorLayout mCoordinatorLayout;
  @BindView(R.id.toolbar)
  Toolbar mToolbar;
  @BindView(R.id.fab)
  FloatingActionButton mFab;

  @BindView(R.id.profile_placeholder)
  RelativeLayout mProfilePlaceholder;
  @BindView(R.id.app_bar_layout)
  AppBarLayout mAppBarLayout;

  @BindView(R.id.collapsing_toolbar)
  CollapsingToolbarLayout mCollapsingToolbar;

  @BindView(R.id.user_photo_img)
  ImageView mUserPhotoImg;

  @BindView(R.id.tv_user_rating)
  TextView mTvUserRating;
  @BindView(R.id.tv_user_code_lines)
  TextView mTvUserCodeLines;
  @BindView(R.id.tv_user_projects)
  TextView mTvUserProjects;
  List<TextView> userValuesView;

  private AppBarLayout.LayoutParams mAppBarParams = null;

  @BindView(R.id.user_phone_et)
  EditText mUserPhoneEt;
  @BindView(R.id.user_email_et)
  EditText mUserEmailEt;
  @BindView(R.id.user_vk_et)
  EditText mUserVkEt;
  @BindView(R.id.user_git_et)
  EditText mUserGitEt;
  @BindView(R.id.user_bio_et)
  EditText mUserBioEt;

  @BindView(R.id.user_phone_til)
  TextInputLayout mUserPhoneTil;
  @BindView(R.id.user_email_til)
  TextInputLayout mUserEmailTil;
  @BindView(R.id.user_vk_til)
  TextInputLayout mUserVkTil;
  @BindView(R.id.user_git_til)
  TextInputLayout mUserGitTil;
  @BindView(R.id.user_about_til)
  TextInputLayout mUserAboutTil;


  @BindView(R.id.make_call_img)
  ImageView mMakeCallImg;
  @BindView(R.id.send_mail_img)
  ImageView mSendMailImg;
  @BindView(R.id.show_vk_link_img)
  ImageView mShowVkLinkImg;
  @BindView(R.id.show_git_link_img)
  ImageView mShowGitLinkImg;

  private List<EditText> mUserInfoViews;

  //    Создаем фотофайл
  private File mPhotoFile = null;
  private Uri mSelectedImage = null;

  private boolean mPhotoIsChanged = false;

  private UserModelResponse userModelResponse;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    mDataManager = DataManager.getInstance();

    mFab.setOnClickListener(this);
    mProfilePlaceholder.setOnClickListener(this);

    //    Clickable imageView
    mMakeCallImg.setOnClickListener(this);
    mSendMailImg.setOnClickListener(this);
    mShowVkLinkImg.setOnClickListener(this);
    mShowGitLinkImg.setOnClickListener(this);

    mUserInfoViews = new ArrayList<>();
    mUserInfoViews.add(mUserPhoneEt);
    mUserInfoViews.add(mUserEmailEt);
    mUserInfoViews.add(mUserVkEt);
    mUserInfoViews.add(mUserGitEt);
    mUserInfoViews.add(mUserBioEt);

    userValuesView = new ArrayList<>();
    userValuesView.add(mTvUserRating);
    userValuesView.add(mTvUserCodeLines);
    userValuesView.add(mTvUserProjects);

    setupToolbar();
    setupDrawer();
    initUserFields();
    initUserInfoValue();

    showUserPhotoWithPlaceholder();

    setValidators();

    //    Проверка состояния Активити
    if (savedInstanceState == null) {
      //      Активити запускается впервые
    } else {
      //      Активити уже создавалось
      //    Задаем значение по умолчанию 0, если 1, то нужно вкючить режим редактирования
      mCurrentEditMode = savedInstanceState.getInt(ConstantManager.EDIT_MODE_KEY, 0);
      changeEditMode(mCurrentEditMode);
    }


  }

  //  получаем Instance Picasso c уже готовыми настройками кеширования и контекста
  private void showUserPhotoWithPlaceholder() {
    Display display = getWindowManager().getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    int width = size.x;

    Picasso.with(this)
        .load(mDataManager.getPreferencesManager().loadUserPhoto())
        .placeholder(R.drawable.userphoto)
        .resize(width, getResources().getDimensionPixelOffset(R.dimen.size_profile_image))
        .centerCrop()
        .into(mUserPhotoImg);
  }

  @Override
  protected void onPause() {
    super.onPause();
    saveUserInfoValues();
  }

  /**
   * Получение результата из другой Активити (фото из камеры или галереи)
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case ConstantManager.REQUEST_GALLERY_PICTURE:
        if (resultCode == RESULT_OK && data != null) {
          mSelectedImage = data.getData();

          insertProfileImage(mSelectedImage);
        }
        break;
      case ConstantManager.REQUEST_CAMERA_PICTURE:
        if (resultCode == RESULT_OK && mPhotoFile != null) {
          mSelectedImage = Uri.fromFile(mPhotoFile);

          insertProfileImage(mSelectedImage);
        }
        break;
    }
  }

  private void insertProfileImage(Uri selectedImage) {
    Display display = getWindowManager().getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    int width = size.x;

    Picasso.with(this)
        .load(selectedImage)
        .placeholder(R.drawable.userphoto)
        .resize(width, getResources().getDimensionPixelOffset(R.dimen.size_profile_image))
        .centerCrop()
        .memoryPolicy(MemoryPolicy.NO_CACHE)
        .into(mUserPhotoImg);

    mDataManager.getPreferencesManager().saveUserPhoto(selectedImage);
    mPhotoIsChanged = true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      mDrawerLayout.openDrawer(GravityCompat.START);
    }
    return super.onOptionsItemSelected(item);
  }

  //  Инициализация Тулбара
  private void setupToolbar() {
    //    Меняем ActionBar на toolbar
    setSupportActionBar(mToolbar);

    ActionBar actionBar = getSupportActionBar();

    mAppBarParams = (LayoutParams) mCollapsingToolbar.getLayoutParams();

    if (actionBar != null) {
      actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  //  Инициализация Дровера
  private void setupDrawer() {
    mNavigationView.setNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        showSnackbar(item.getTitle().toString());
        item.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return false;
      }
    });
  }


  //  Метод переключает состояние View фокус / не в фокусе из перебранных EditText в массиве
  private void changeEditMode(int mode) {
    if (mode == 1) {
      mFab.setImageResource(R.drawable.ic_done_white_24dp);
      for (EditText userValue : mUserInfoViews) {
        userValue.setEnabled(true);
        userValue.setFocusable(true);
        userValue.setFocusableInTouchMode(true);

        showProfilePlaceholder();
        lockToolbar();
        //        Делаем невидимым заголовок в режиме редактирования
        mCollapsingToolbar.setExpandedTitleColor(Color.TRANSPARENT);
      }
    } else {
      mFab.setImageResource(R.drawable.ic_edit_white_24dp);
      for (EditText userValue : mUserInfoViews) {
        userValue.setEnabled(false);
        userValue.setFocusable(false);
        userValue.setFocusableInTouchMode(false);

        hideProfilePlaceholder();
        unlockToolbar();
        mCollapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.white));

        saveUserInfoValues();
      }
    }
  }

  //  Инициализация Снекбара
  private void showSnackbar(String message) {
    Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
  }

  //  @OnClick(R.id.fab)
  //  public void fabClick(View v) {
  //
  //    switch (v.getId()) {
  //      case R.id.fab:
  //        showSnackbar("click");
  //        if (mCurrentEditMode == 0) {
  //          changeEditMode(1);
  //          mCurrentEditMode = 1;
  //        } else {
  //          changeEditMode(0);
  //          mCurrentEditMode = 0;
  //        }
  //        break;
  //    }
  //  }

  //  Для сохранения состояние режима редактирования
  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt(ConstantManager.EDIT_MODE_KEY, mCurrentEditMode);
  }

  @Override
  public void onClick(View v) {
    Intent intent;
    List<ResolveInfo> resInfo;
    String url;
    switch (v.getId()) {
      case R.id.fab:

        if (mCurrentEditMode == 0) {
          showSnackbar("edit mode");
          changeEditMode(1);
          mCurrentEditMode = 1;

        } else {
          showSnackbar("done");
          changeEditMode(0);
          mCurrentEditMode = 0;
          saveUserInfoValues();
        }
        break;

      case R.id.profile_placeholder:
        showDialog(ConstantManager.LOAD_PROFILE_PHOTO);
        break;
      //        Clickable ImageView
      case R.id.make_call_img:
        intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mDataManager.getPreferencesManager()
            .loadUserProfileData()
            //            Получаем первый элемент массива данных пользователя, это и будет номер телефона
            .get(0), null));
        startActivity(intent);
        break;
      case R.id.send_mail_img:
        intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("message/rfc822");
        intent.setData(Uri.fromParts("mailto", mDataManager.getPreferencesManager().loadUserProfileData().get(1), null));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Тема:");
        intent.putExtra(Intent.EXTRA_TEXT, "Введите текст сообщения...");

        // Проверяем, есть ли хоть одно приложение, способное обработать отправку писем
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;

        //        Если такие Активити есть, то отправляем интент на запуск
        if (isIntentSafe) {
          startActivity(intent);
        } else {
          Toast.makeText(getApplicationContext(), "Не установлен почтовый клиент!", Toast.LENGTH_LONG).show();
          startActivity(Intent.createChooser(intent, "Отправка письма..."));
        }

        break;
      case R.id.show_vk_link_img:
        final String VK_APP_PACKAGE_ID = "com.vk.android";
        url = "https://" + mUserInfoViews.get(ConstantManager.PROFILE_ET_VK_POSITION).getText().toString();
        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        // Проверяем, есть ли хоть одно приложение, способное обработать этот Интент
        resInfo = getPackageManager().queryIntentActivities(intent, 0);
        // Если такого Активити нет, то выводим Снекбар с описанием проблемы
        if (resInfo.isEmpty()) {
          Snackbar.make(mCoordinatorLayout, R.string.string_warning_does_not_match_activity, Snackbar.LENGTH_LONG).show();
        } else {
          for (ResolveInfo info : resInfo) {
            if (VK_APP_PACKAGE_ID.equals(info.activityInfo.packageName)) {
              intent.setPackage(info.activityInfo.packageName);
            }
          }
          startActivity(intent);
        }
        break;
      case R.id.show_git_link_img:
        url = "https://" + mUserInfoViews.get(ConstantManager.PROFILE_ET_GITHUB_POSITION).getText().toString();
        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        // Проверяем, есть ли хоть одно приложение, способное обработать этот Интент
        resInfo = getPackageManager().queryIntentActivities(intent, 0);
        // Если такого Активити нет, то выводим Снекбар с описанием проблемы
        if (resInfo.isEmpty()) {
          Snackbar.make(mCoordinatorLayout, R.string.string_warning_does_not_match_activity, Snackbar.LENGTH_LONG).show();
        } else {
          startActivity(intent);
        }
        break;
    }
  }

  private void setValidators() {
    final String PHONE_REGEXP = "^[\\d\\(\\)\\-+ ]{11,20}$";
    final String EMAIL_REGEXP = "^[A-Za-z0-9+_.-]{3,}+@([A-Za-z0-9+_.-]{2,})+\\.+[a-zA-Z]{2,}$";
    final String VK_REGEXP = "^vk\\.com\\/[\\w]{3,}+$";
    final String GITHUB_REGEXP = "^github\\.com\\/[\\w]{3,}+$";

    mUserInfoViews.get(ConstantManager.PROFILE_ET_PHONE_POSITION).addTextChangedListener(new ProfileDataTextWatcher(Pattern.compile(PHONE_REGEXP), mUserPhoneTil));
    mUserInfoViews.get(ConstantManager.PROFILE_ET_EMAIL_POSITION).addTextChangedListener(new ProfileDataTextWatcher(Pattern.compile(EMAIL_REGEXP), mUserEmailTil));
    mUserInfoViews.get(ConstantManager.PROFILE_ET_VK_POSITION).addTextChangedListener(new ProfileDataTextWatcher(Pattern.compile(VK_REGEXP), mUserVkTil));
    mUserInfoViews.get(ConstantManager.PROFILE_ET_GITHUB_POSITION).addTextChangedListener(new ProfileDataTextWatcher(Pattern.compile(GITHUB_REGEXP), mUserGitTil));
  }

  private void initUserFields() {
    List<String> userData = mDataManager.getPreferencesManager().loadUserProfileData();
    //    Обращаемся к массиву mUserInfoViews, получаем элемент i, и вставляем текст, который приходит из userData
    for (int i = 0; i < userData.size(); i++) {
      mUserInfoViews.get(i).setText(userData.get(i));
    }
  }

  private void saveUserInfoValues() {
    List<String> userData = new ArrayList<>();
    for (EditText userFieldView : mUserInfoViews) {
      userData.add(userFieldView.getText().toString());
    }
    mDataManager.getPreferencesManager().saveUserProfileData(userData);
  }
//  private void saveUserInfoValues() {
//    String[] userFields = {
//        userModelResponse.getData().getUser().getContacts().getPhone(),
//        userModelResponse.getData().getUser().getContacts().getEmail(),
//        userModelResponse.getData().getUser().getContacts().getVk(),
//        userModelResponse.getData().getUser().getPublicInfo().getBio()
//    };
//
//    mDataManager.getPreferencesManager().saveUserProfileData(userFields);
//  }

  private void initUserInfoValue() {
    List<String> userData = mDataManager.getPreferencesManager().loadUserProfileValues();
    for (int i = 0; i < userData.size(); i++) {
      userValuesView.get(i).setText(userData.get(i));
    }
  }

  private void loadPhotoFromGallery() {
    //    Пробуем достать данные по средством "ПИК" - выбора
    Intent takeGalleryIntent = new Intent(Intent.ACTION_PICK, Media.EXTERNAL_CONTENT_URI);
    //    Указывем какой тип хотим получить
    takeGalleryIntent.setType("image/*");

    startActivityForResult(Intent.createChooser(takeGalleryIntent, getString(R.string.user_profile_chose_message)), ConstantManager.REQUEST_GALLERY_PICTURE);
  }

  /**
   * Для версии Android 24 и выше, нужно применить этот хак для кореектной работы камеры
   * Uri photoURI = FileProvider.getUriForFile(getApplicationContext(), this.getApplicationContext().getPackageName() + ".ru.echodc.devintensive.provider", mPhotoFile);
   *
   * вместо - Uri.fromFile(mPhotoFile) -- так НЕ РАБОТАЕТ!!!
   */
  private void loadPhotoFromCamera() {
    //    Проверяем разрешено ли разрешение на камеру и на запись в съемный носитель в пакете настроек
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

      //    Создаем Интент
      Intent takeCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      try {
        //      Пробуем передать файл с фоткой
        mPhotoFile = createImageFile();
      } catch (IOException e) {
        e.printStackTrace();
        Snackbar.make(mCoordinatorLayout, R.string.error_create_tempfile, Snackbar.LENGTH_LONG).show();
      }

      if (mPhotoFile != null) {
        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(), this.getApplicationContext().getPackageName() + ".ru.echodc.devintensive.provider", mPhotoFile);
        takeCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        //        takeCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPhotoFile));
        startActivityForResult(takeCaptureIntent, ConstantManager.REQUEST_CAMERA_PICTURE);
      }
    } else {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
          ConstantManager.CAMERA_REQUEST_PERMISSION_CODE);

      Snackbar.make(mCoordinatorLayout, "Для корректной работы приложения необходимо дать разрешение!", Snackbar.LENGTH_LONG)
          .setAction("Разрешить!", new OnClickListener() {
            @Override
            public void onClick(View view) {
              //              Перенаправляем пользователя в настройки приложения
              openApplicationSettings();
            }
          }).show();
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

    if (requestCode == ConstantManager.CAMERA_REQUEST_PERMISSION_CODE && grantResults.length == 2) {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED
          && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        loadPhotoFromCamera();
      }
    } else if (requestCode == ConstantManager.GALLERY_REQUEST_PERMISSION_CODE && grantResults.length == 1) {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        loadPhotoFromGallery();
      }
    }
  }

  private void hideProfilePlaceholder() {
    mProfilePlaceholder.setVisibility(View.GONE);

  }

  private void showProfilePlaceholder() {
    mProfilePlaceholder.setVisibility(View.VISIBLE);
  }

  //  Чтобы Toolbar не схлопывался
  private void lockToolbar() {
    mAppBarLayout.setExpanded(true, true);
    mAppBarParams.setScrollFlags(0);
    mCollapsingToolbar.setLayoutParams(mAppBarParams);
  }

  private void unlockToolbar() {
    mAppBarParams.setScrollFlags(LayoutParams.SCROLL_FLAG_SCROLL | LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED);
    mCollapsingToolbar.setLayoutParams(mAppBarParams);
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    switch (id) {
      case ConstantManager.LOAD_PROFILE_PHOTO:
        final String[] selectItems = {getString(R.string.user_profile_dialog_gallery), getString(R.string.user_profile_dialog_camera), getString(R.string.user_profile_dialog_cancel)};

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.user_profile_dialog_title);
        builder.setItems(selectItems, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int choiceItem) {
            switch (choiceItem) {
              case 0:
                //                Загрузить из галереи
                loadPhotoFromGallery();
                break;
              case 1:
                //                Сделать снимок
                loadPhotoFromCamera();
                break;
              case 2:
                //                отменить
                dialog.cancel();
                break;
            }
          }
        });
        return builder.create();
      default:
        return null;
    }

  }

  private File createImageFile() throws IOException {
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageFileName = "JPEG_" + timeStamp + "_";
    File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

    //    Создаем сам файл с фоткой и указываем место для хранения
    File image = File.createTempFile(imageFileName, ".jpg", storageDir);

    //    Для Android 6 & up, чтобы фото сохранялись в галерею и корректно отображалось время записи
    ContentValues values = new ContentValues();
    values.put(Media.DATE_TAKEN, System.currentTimeMillis());
    values.put(Media.MIME_TYPE, "image/jpeg");
    values.put(MediaColumns.DATA, image.getAbsolutePath());

    this.getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);

    return image;
  }

  //  Вспомогательный метод для определения настроек
  public void openApplicationSettings() {
    //    Передаем в Интент имя пакета с настройками
    Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
    startActivityForResult(appSettingsIntent, ConstantManager.CAMERA_REQUEST_PERMISSION_CODE);
  }
}
