package ru.echodc.devintensive.data.network;

import android.content.Context;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Builder;

public class PicassoCache {

  private Context mContext;
  private Picasso mPicassoInstance;

  public PicassoCache(Context context) {
    this.mContext = context;
    //    Подключаем библиотеку
    OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context, Integer.MAX_VALUE);
    //    Добавляем в билдер Downloader
    Picasso.Builder builder = new Builder(context);
    builder.downloader(okHttp3Downloader);

    //    Делаем Синглтон из Пикассо
    mPicassoInstance = builder.build();
    Picasso.setSingletonInstance(mPicassoInstance);
  }

  public Picasso getPicassoInstance() {
    if (mPicassoInstance == null) {
      new PicassoCache(mContext);
      return mPicassoInstance;
    }
    return mPicassoInstance;
  }
}
