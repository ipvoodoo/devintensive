package ru.echodc.devintensive.data.network.request;

public class UserLoginRequest {

  private String email;
  private String password;

  public UserLoginRequest(String email, String password) {
    this.email = email;
    this.password = password;
  }
}
