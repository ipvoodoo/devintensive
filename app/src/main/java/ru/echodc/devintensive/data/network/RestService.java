package ru.echodc.devintensive.data.network;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import ru.echodc.devintensive.data.network.request.UserLoginRequest;
import ru.echodc.devintensive.data.network.response.UploadPhotoResponse;
import ru.echodc.devintensive.data.network.response.UserListResponse;
import ru.echodc.devintensive.data.network.response.UserModelResponse;

public interface RestService {

  //  Заголовки
  @Headers({
      "Custom-Header: my header Value"
  })

  //  С заголовком
  //  @POST("login")
  //  Call<UserModelResponse> loginUser(@Header("Last-Modified") String lastMod,
  //      @Body UserLoginRequest request);

  //  Без заголовка
  @POST("login")
  Call<UserModelResponse> loginUser(@Body UserLoginRequest request);

  @Multipart
  @POST("user/{userId}/publicValues/profilePhoto")
  Call<UploadPhotoResponse> uploadPhoto(@Path("userId") String userId, @Part MultipartBody.Part file);

  //  Возвращает список пользователе   на сервере
  @GET("user/list?orderBy=rating")
  Call<UserListResponse> getUserList();

}
