package ru.echodc.devintensive.data.network.interceptors;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.echodc.devintensive.data.managers.DataManager;
import ru.echodc.devintensive.data.managers.PreferencesManager;

public class HeaderInterceptor implements Interceptor {

  @Override
  public Response intercept(Chain chain) throws IOException {
    PreferencesManager preferenceManager = DataManager.getInstance().getPreferencesManager();

    //    Шлем оригинальный запрос
    Request original = chain.request();

    //    Создаем билдер для встаки в запрос доп. параметров
    Request.Builder requestBuilder = original.newBuilder()
        .header("X-Access-Token", preferenceManager.getAuthToken())
        .header("Request-User-Id", preferenceManager.getUserId())
        .header("User-Agent", "DevIntensiveApp")
        .header("Cache-Control", "max-age=" + (60 * 60 * 24));

    Request request = requestBuilder.build();

    //    Возвращаем подписаный запрос с новыми заголовками
    return chain.proceed(request);
  }
}
