package ru.echodc.devintensive.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
import ru.echodc.devintensive.data.network.response.UserModelResponse.Contacts;
import ru.echodc.devintensive.data.network.response.UserModelResponse.ProfileValues;
import ru.echodc.devintensive.data.network.response.UserModelResponse.PublicInfo;
import ru.echodc.devintensive.data.network.response.UserModelResponse.Repositories;

public class UserListResponse {

  @SerializedName("success")
  @Expose
  public boolean success;

  @SerializedName("data")
  @Expose
  public List<UserData> mUserData = new ArrayList<>();

  public List<UserData> getUserData() {
    return mUserData;
  }

  public class UserData {


    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("second_name")
    @Expose
    private String secondName;
    @SerializedName("__v")
    @Expose
    private int v;
    @SerializedName("repositories")
    @Expose
    private UserModelResponse.Repositories repositories;
    @SerializedName("contacts")
    @Expose
    private Contacts contacts;
    @SerializedName("profileValues")
    @Expose
    private UserModelResponse.ProfileValues profileValues;
    @SerializedName("publicInfo")
    @Expose
    private UserModelResponse.PublicInfo publicInfo;
    @SerializedName("specialization")
    @Expose
    private String specialization;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("updated")
    @Expose
    private String updated;

    public Repositories getRepositories() {
      return repositories;
    }

    public ProfileValues getProfileValues() {
      return profileValues;
    }

    public PublicInfo getPublicInfo() {
      return publicInfo;
    }

    public String getFullName() {
      return firstName + " " + secondName;
    }

    public String getId() {
      return id;
    }
  }
}
