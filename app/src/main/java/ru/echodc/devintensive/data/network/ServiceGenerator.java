package ru.echodc.devintensive.data.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.echodc.devintensive.data.network.interceptors.HeaderInterceptor;
import ru.echodc.devintensive.utils.AppConfig;
import ru.echodc.devintensive.utils.DevintensiveApplication;

public class ServiceGenerator {

  //  Строим HTTP клиент
  private static OkHttpClient.Builder httpClient = new Builder();

  private static Retrofit.Builder sBuilder = new Retrofit.Builder()
      .baseUrl(AppConfig.BASE_URL)
      .addConverterFactory(GsonConverterFactory.create());

  public static <S> S createService(Class<S> serviceClass) {
    //    Подключаме логгирование
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    loggingInterceptor.setLevel(Level.BODY);

    httpClient.addInterceptor(new HeaderInterceptor());
    httpClient.addInterceptor(loggingInterceptor);
    httpClient.connectTimeout(AppConfig.MAX_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
    httpClient.readTimeout(AppConfig.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS);
    httpClient.cache(new Cache(DevintensiveApplication.getContext().getCacheDir(), Integer.MAX_VALUE));
    httpClient.addInterceptor(new StethoInterceptor());

    Retrofit retrofit = sBuilder
        .client(httpClient.build())
        .build();
    return retrofit.create(serviceClass);
  }

}
