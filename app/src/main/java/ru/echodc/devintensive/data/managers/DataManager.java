package ru.echodc.devintensive.data.managers;


import android.content.Context;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import ru.echodc.devintensive.data.network.PicassoCache;
import ru.echodc.devintensive.data.network.RestService;
import ru.echodc.devintensive.data.network.ServiceGenerator;
import ru.echodc.devintensive.data.network.request.UserLoginRequest;
import ru.echodc.devintensive.data.network.response.UserListResponse;
import ru.echodc.devintensive.data.network.response.UserModelResponse;
import ru.echodc.devintensive.data.network.response.UserModelResponse.User;
import ru.echodc.devintensive.data.storage.models.DaoSession;
import ru.echodc.devintensive.utils.DevintensiveApplication;

public class DataManager {

  private static DataManager INSTANCE = null;
  private Picasso mPicasso;

  private Context mContext;

  private PreferencesManager mPreferencesManager;

  private RestService mRestService;

  private DaoSession mDaoSession;

  public DataManager() {
    this.mPreferencesManager = new PreferencesManager();
    this.mContext = DevintensiveApplication.getContext();
    this.mRestService = ServiceGenerator.createService(RestService.class);
    //    Для автоматического кеширования, скачанных данных
    this.mPicasso = new PicassoCache(mContext).getPicassoInstance();
    this.mDaoSession = DevintensiveApplication.getDaoSession();
  }

  public PreferencesManager getPreferencesManager() {
    return mPreferencesManager;
  }

  public Context getContext() {
    return mContext;
  }

  public Picasso getPicasso() {
    return mPicasso;
  }

  public static DataManager getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new DataManager();
    }
    return INSTANCE;
  }

  // region ===================== Network =====================
  //  С заголовком с параметрах
  //  public Call<UserModelResponse> loginUser(String lastModified, UserLoginRequest userLoginRequest ) {

  //  Без заголовка
  public Call<UserModelResponse> loginUser(UserLoginRequest userLoginRequest) {
    return mRestService.loginUser(userLoginRequest);
  }

  public Call<UserListResponse> getUserListFromNetwork() {
    return mRestService.getUserList();
  }
  // endregion Network

  // region ===================== Database =====================


  public DaoSession getDaoSession() {
    return mDaoSession;
  }

  public List<User> getUserListFromDb() {
    List<User> temp = new ArrayList<>();
    return temp;
  }
  // endregion Database
}
